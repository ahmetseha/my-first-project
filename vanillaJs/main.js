// COUNTER

const counter = document.getElementById("counter");
const increase = document.querySelector(".increase");
const decrease = document.querySelector(".decrease");
const reset = document.querySelector(".reset");
let count = 0;
increase.addEventListener("click", () => {
  count++;
  counter.innerHTML = count;
  if (count > 0) {
    counter.style.color = "#E85D04";
  }
  if (count == 0) {
    counter.style.color = "#457b9d";
  }
});
decrease.addEventListener("click", () => {
  count--;
  counter.innerHTML = count;
  if (count < 0) {
    counter.style.color = "#6A040F";
  }
  if (count == 0) {
    counter.style.color = "#457b9d";
  }
});
reset.addEventListener("click", () => {
  count = 0;
  counter.innerHTML = count;
  if (count == 0) {
    counter.style.color = "#457b9d";
  }
});

//CHANGE COLOR

const hexCodeText = document.querySelector("#hexcode");
const randomBtn = document.querySelector(".randomBtn");
const changeColorContainer = document.querySelector("#changeColor");

const randomColor = () => {
  let hexCode = "#" + Math.floor(Math.random() * 16777215).toString(16);
  return hexCode;
};

const randomColorClick = () => {
  const colorHexCode = randomColor();
  changeColorContainer.style.backgroundColor = colorHexCode;
  hexCodeText.textContent = colorHexCode;
};

//FORM VALİDATİONS

// const form = document.querySelector("#formId");
// const formSubmitBtn = document.querySelector("#formSubmitBtn");

// const validations = (form) => {
//   let invalids = form.querySelectorAll(":invalid").length;
//   console.log(invalids);

//   if (invalids === 0) {
//     formSubmitBtn.removeAttribute("disabled");
//   } else {
//     formSubmitBtn.setAttribute("disabled", "disabled");
//   }
// };
// validations();
